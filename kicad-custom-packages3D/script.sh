#!/bin/bash
NEW_PATH="/ssd/rnd2/kicad/paths/kicad-custom-packages3D"
for i in */; do
    echo "Folder: $i"
    if [ -d "$i/walter" ]; then
        cd $i/walter
        for f in *; do
            mv $f "${NEW_PATH}/$f"
        done
        cd ../..
    fi
done
