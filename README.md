KiCAD libraries
----

This repo contains various KiCAD symbols, footprints amd 3D packages.
This is complementary to:
* https://github.com/KiCad/kicad-library
* https://github.com/KiCad/kicad-symbols
* https://github.com/KiCad/kicad-packages3D

Therefore, you need to install to different paths and them import under
new paths in `Environment Variable Configuration`. For example:

- | -
Name | Path
KISYSMOD | /path/to/kicad-custom-footprints
KISYSMOD3D | /path/to/kicad-custom-packages3D
KICAD_CUSTOM_SYMBOLS | /path/to/kicad-custom-symbols
